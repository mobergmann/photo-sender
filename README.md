# Roadmap
- [x] socket prototype
- [x] wrap in classes
- [x] add sending byte array functionality
- [x] send photo
- [ ] take photo
- [ ] add tls
- [ ] wrap tls in classes
