#ifndef SOCKET_H
#define SOCKET_H

#include <vector>
#include <string>
#include <cstddef>
#include <stdexcept>

#include "custom_types.h"

namespace socket_wrapper
{

using std::vector;
using std::string;
using std::byte;

using i16 = short;
using u16 = unsigned short;
using i32 = int;
using u32 = unsigned int;
using i64 = long;
using u64 = unsigned long;

/// a sequence of bytes
using bytes = vector<byte>;

/// file-descriptor for identifying the c-sockets
using filedescriptor_t = int;

/**
 * converts a given object into bytes
 * @tparam T type of the conversion object
 * @param t object to convert
 * @return the given object converted to bytes
 */
template <typename T>
bytes to_bytes(const T& t)
{
  const size_t length = sizeof(t);
  bytes b(length);

  const auto tmp = static_cast<const byte*>(static_cast<const void*>(&t)); // cast as byte array
  for (size_t i = 0; i < length; ++i)
  {
    b[i] = tmp[i];
  }

  return b;
}

/**
 * converts a given object into a specified type
 * @tparam T type into which bytes should be converted
 * @param b bytes to be converted
 * @return the given byte sequence in bytes
 */
template <typename T>
T from_bytes(const bytes& b) noexcept(false)
{
  const size_t length = b.size();
  byte array[length] {};

  // if unequal binary size, then the given type and bytes are not equal
  if (sizeof(T) != b.size())
  {
    throw std::runtime_error("size of the given type and size of the byte sequence are not equal and can therefore not be the same");
  }

  // to in memory array
  for (size_t i = 0; i < length; ++i)
  {
    array[i] = b[i];
  }

  T t = *reinterpret_cast<T*>(array);
  return t;
}

/**
 * wrapper class for the c socket functions.
  * @disclaimer this class only supports TCP
  */
class Socket
{
  /// c socket file descriptor
  filedescriptor_t _sockfd;

 public:
  Socket();

  ~Socket();

  /**
   * method for connecting a client-socket to a server-socket by an ip-address and an port.
   * wrapper for the c connect function.
   * for more information see the `connect` manpage.
   * @param ip_address ip-address of the server-socket
   * @param port port of the server-socket
   * @return the c-filedescriptor of the connected socket
   */
  filedescriptor_t connect(const string& ip_address, const u16& port);

  /**
   * method for binding a server-socket to an port.
   * wrapper for the c bind function.
   * for more information see the `bind` manpage.
   * @param port port to connect to
   */
  void bind(const u16& port);

  /**
   * method for start listening on a server-socket.
   * wrapper for the c connect function.
   * for more information see the `listen` manpage.
   * @param connection_limit the maximum amount of simultaneous connections
   */
  void listen(int connection_limit);

  /**
   * method for accepting an incoming connection to the socket. warning, this method is blocking.
   * wrapper for the c accept function.
   * for more information see the `accept` manpage.
   * @return
   */
  filedescriptor_t accept();

  /**
   * sends the given data trough the socket identified by the filedescriptor.
   * for more information see the `send` manpage.
   * @param data data vector
   * @param sockfd filedescriptor of the socket, to which data should be send
   *    (returned either by the `accept`, or the `connect` method)
   */
  static void send_all(const vector<byte>& data, const filedescriptor_t sockfd);

  /**
   * receives at maximum a given amount of data identified by the filedescriptor.
   * for more information see the `recv` manpage.
   * @param n amount of data to receive
   * @param sockfd filedescriptor of the socket, to which data should be send
   *    (returned either by the `accept`, or the `connect` method)
   * @return the received data in a byte vector
   */
  static vector<byte> receive(size_t n, const filedescriptor_t sockfd);
};

}

#endif // SOCKET_H
