#include "Socket.h"

#include <vector>
#include <string>
#include <cstddef>
#include <stdexcept>
#include <thread>
#include <cerrno>

#include <arpa/inet.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

namespace socket_wrapper
{

using std::byte;
using std::string;
using std::vector;

Socket::Socket()
{
  // construct socket file descriptor (ipv4 and TCP)
  _sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
  if (_sockfd < 0)
    throw std::runtime_error("could not create socket: " + string(strerror(errno)));
}

Socket::~Socket()
{
  ::close(_sockfd);
}

filedescriptor_t Socket::connect(const string& ip_address, const u16& port)
{
  struct ::sockaddr_in serv_addr;
  memset(&serv_addr, '0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if (::inet_pton(AF_INET, ip_address.c_str(), &serv_addr.sin_addr) <= 0)
    throw std::runtime_error("error in inet_pton call: " + string(strerror(errno)));

  if (::connect(_sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    throw std::runtime_error("Could not connect to server: " + string(strerror(errno)));

  return _sockfd;
}

void Socket::bind(const u16& port)
{
  // construct address
  struct sockaddr_in my_addr;
  my_addr.sin_family = AF_INET;
  my_addr.sin_port = htons(port);
  my_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  // bind socket to address
  if (::bind(_sockfd, (struct sockaddr *)&my_addr, sizeof(my_addr)) < 0)
    throw std::runtime_error("could not bind the socket: " + string(strerror(errno)));
}

void Socket::listen(int connection_limit)
{
  // listen for incoming connections
  if (::listen(_sockfd, connection_limit) < 0)
    throw std::runtime_error("could not listen: " + string(strerror(errno)));
}

filedescriptor_t Socket::accept()
{
  struct ::sockaddr conn_addr;
  ::socklen_t conn_addr_size;
  int connfd = ::accept(_sockfd, (struct ::sockaddr *)&conn_addr, &conn_addr_size);
  if (connfd < 0)
    throw std::runtime_error("could not accept connection: " + std::to_string(errno) + " " + string(strerror(errno)));

  return connfd;
}

void Socket::send_all(const vector<byte>& data, const filedescriptor_t sockfd)
{
  size_t send_data = 0;

  while (send_data < data.size())
  {
    size_t tmp = ::send(sockfd, &data[send_data], data.size() - send_data, 0);
    if (tmp < 0) // throw on error
      throw std::runtime_error("error while receiving data from socket: " + string(strerror(errno)));

    send_data += tmp;
  }
}

vector<byte> Socket::receive(size_t n, const filedescriptor_t sockfd)
{
  vector<byte> buffer(n, byte('\0'));
  size_t send_size = 0;

  while (send_size < n)
  {
    size_t tmp = ::recv(sockfd, &buffer[0] + send_size, n - send_size, 0);
    if (tmp < 0) // throw on error
      throw ::std::runtime_error("error while receiving data from socket: " + string(strerror(errno)));
    if (tmp == 0) // on EOF
      break;

    // increment already send amount of data
    send_size += tmp;
  }

  return buffer;
}

}
