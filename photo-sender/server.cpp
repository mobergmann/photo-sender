#include "../wrapper/Socket.h"

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <fstream>
#include <vector>
#include <string>
#include <thread>

using std::byte;
using std::string;
using std::vector;

using socket_wrapper::u16;
using socket_wrapper::bytes;
using socket_wrapper::Socket;
using socket_wrapper::to_bytes;
using socket_wrapper::from_bytes;
using socket_wrapper::filedescriptor_t;

void write_file(const bytes& data, const string& path)
{
  std::ofstream out(path, std::ios_base::out | std::ios_base::binary);
  out.write((char*) &data[0], data.size());
}

void server_background_task(const filedescriptor_t client_fd, const size_t photo_index)
{
  const string out_path = "/home/mobergmann/Downloads/out/" + std::to_string(photo_index) + ".jpg";

  auto data = Socket::receive(sizeof(size_t), client_fd);
//  size_t length = *(size_t*)(&data[0]); // cast received bytes to size_t
  auto length = from_bytes<size_t>(data);

  auto file_data = Socket::receive(length, client_fd);
  std::ofstream out(out_path, std::ios_base::out | std::ios_base::binary);
  write_file(file_data, out_path);
}


int main()
{
  size_t photos = 0;
  const u16 port = 8000;

  Socket server;
  try
  {
    server.bind(port);
    server.listen(10);
    while (true) {
      filedescriptor_t client_fd = server.accept();
      new std::thread(server_background_task, client_fd, photos++);
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}