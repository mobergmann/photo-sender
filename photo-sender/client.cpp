#include "../wrapper/Socket.h"

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <fstream>
#include <vector>
#include <thread>

using std::byte;
using std::string;
using std::vector;

using socket_wrapper::u16;
using socket_wrapper::bytes;
using socket_wrapper::Socket;
using socket_wrapper::to_bytes;
using socket_wrapper::filedescriptor_t;

/**
 * converts string to byte vector
 * @param in input sequence
 * @return byte vector representing the input sequence
 */
bytes read_file(const string& path)
{
  // prepare binary data to be sent
  std::ifstream in(path, std::ios_base::in | std::ios_base::binary);

  // get length of file
  in.seekg(0, std::ios::end);
  const long length = in.tellg();
  in.seekg(0, std::ios::beg);

  bytes ret(length);
  in.read(reinterpret_cast<char*>(&ret[0]), length);
  return ret;
}

int main()
{
  const string in_path = "/home/mobergmann/Downloads/test-in.jpg";
  const string domain = "127.0.0.1";
  const u16 port = 8000;

  Socket client;
  try
  {
    // read file
    std::cout << "Reading file...";
    bytes file = read_file(in_path);
    std::cout << " Done." << std::endl;

    filedescriptor_t server_fd = client.connect(domain, port);

    // send photo via client
    std::cout << "Sending file...";

    // send number of incoming bytes{
    auto converted = socket_wrapper::to_bytes<size_t>(static_cast<size_t>(file.size()));
    Socket::send_all(converted, server_fd);

    Socket::send_all(file, server_fd);
    std::cout << " Done." << std::endl;
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}
